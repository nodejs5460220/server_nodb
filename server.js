import http from 'http';

const server = http.createServer((request, response) => {
  response.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
  response.end('<h1>hello im server</h1>');
});

const PORT = 8080;
const HOST = 'localhost';

server.listen(PORT, HOST, () => {
  console.log(`server is start on http://${HOST}:${PORT}`);
});
