import express from 'express';

// connect to express
const app = express();
const PORT = 8080;
const HOST = 'localhost';

// response url address
app.get('/', (request, response) => {
  response.sendFile(__dirname + 'home.html');
});
app.get('/user/:id', (request, response) => {
  response.send(`user: ${request.params.id}`);
});

// listener server
app.listen(PORT, () => {
  console.log(`server is start on http://${HOST}:${PORT}`);
});
