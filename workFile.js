import fs from 'fs';

/**
 * writeFileSync - synchronic function
 * writeFile - asynchronous function
 */

// // create file
// fs.writeFileSync('some.txt', 'hello');

// // read file
// let result = fs.readFileSync('some.txt', 'utf-8');
// console.log(result);

// // add date from file
// fs.writeFileSync('some.txt', result + '\nPrivet');
// result = fs.readFileSync('some.txt', 'utf-8');
// console.log(result);

// fs.readFile('some.txt', 'utf-8', (err, data) => {
//   fs.writeFile('some.txt', data + ' test', (err, data) => {
//     console.log('all ok', data);
//   });
// });

/******************************************************** */

// create dir
fs.mkdir('txt', () => {
  fs.writeFile('./txt/some2.txt', 'some txt', () => {});
});

// delete file
fs.unlink('./txt/some2.txt', (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
  fs.rmdir('./txt', () => {});
});
