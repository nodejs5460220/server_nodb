import http from 'http';
import fs from 'fs';

const server = http.createServer((request, response) => {
  response.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });

  if (request.url === '/') {
    const stream = fs.createReadStream('./server2/home.html');
    stream.pipe(response);
  }

  if (request.url === '/about') {
    const stream = fs.createReadStream('./server2/about.html');
    stream.pipe(response);
  }
});

const PORT = 8080;
const HOST = 'localhost';

server.listen(PORT, HOST, () => {
  console.log(`server is start on http://${HOST}:${PORT}`);
});
